Please refer to the `docs/`

СКИТ - Софтверски Квалитет и Тестирање

Филип Маркоски

This project tries to follow the typical maven project structure:
https://maven.apache.org/guides/introduction/introduction-to-the-standard-directory-layout.html

Software Quality and Testing Project: Contacts 
- Created by: Filip Markoski, 161528, KNI-A
- Subject: Software quality and testing
- Mentor: PhD. Bojana Koteska
- Faculty: FINKI, Skopje
- Year: 2019