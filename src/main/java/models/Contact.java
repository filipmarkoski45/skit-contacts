package models;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class Contact {
    protected String date;

    public Contact(String date) {
        this.date = date;
    }

    public Contact(Contact c) {
        this.date = c.date;
    }

    public boolean isNewerThan(Contact c) {
        /*int thisYear = Integer.parseInt(date.substring(0, 4));
        int thatYear = Integer.parseInt(c.date.substring(0, 4));
        int thisMonth = Integer.parseInt(date.substring(5,7));
        int thatMonth = Integer.parseInt(c.date.substring(5,7));
        int thisDay = Integer.parseInt(date.substring(8,10));
        int thatDay = Integer.parseInt(c.date.substring(8,10));
        return (thisYear < thatYear ? false :
    			thisYear > thatYear ? true :
				thisMonth < thatMonth ? false :
				thisMonth > thatMonth ? true :
				thisDay < thatDay ? false :
				thisDay > thatDay ? true : false);*/

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date thisDate = null;
        Date thatDate = null;
        try {
            thisDate = simpleDateFormat.parse(this.date);
            thatDate = simpleDateFormat.parse(c.date);
        } catch (ParseException e) {
            System.out.println("ParseException in isNewerThan");
        }
        return thisDate.compareTo(thatDate) > 0;
    }

    abstract public String getType();
}
