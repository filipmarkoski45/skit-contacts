package models;

public class EmailContact extends Contact {
    private String email;

    public EmailContact(String date, String email) {
        super(date);
        this.email = email;
    }

    public EmailContact(EmailContact e) {
        super(e);
        email = e.email;
    }

    public String getEmail() {
        return email;
    }

    public String getType() {
        return "Email";
    }
}
