package models;

public class Faculty {
    private String name;
    private Student[] students;

    public Faculty(String n, Student[] s) {
        name = n;
        students = new Student[s.length];
        for (int i = 0; i < s.length; i++) {
            students[i] = new Student(s[i]);
        }
    }

    public int countStudentsFromCity(String cityName) {
        int counter = 0;
        for (int i = 0; i < students.length; i++) {
            if (students[i].getCity().equals(cityName)) {
                counter++;
            }
        }
        return counter;
    }

    public Student getStudent(long index) {
        for (int i = 0; i < students.length; i++) {
            if (students[i].getIndex() == index) {
                return students[i];
            }
        }
        return null;
    }

    public double getAverageNumberOfContacts() {
        double sum = 0;
        for (int i = 0; i < students.length; i++) {
            sum += students[i].getNumberContacts();
        }
        return sum / students.length;
    }

    public Student getStudentWithMostContacts() {
        Student maxContacts = students[0];
        for (int i = 1; i < students.length; i++) {
            if (students[i].getNumberContacts() > maxContacts.getNumberContacts()) {
                maxContacts = students[i];
            } else if (students[i].getNumberContacts() == maxContacts.getNumberContacts()) {
                if (students[i].getIndex() > maxContacts.getIndex()) {
                    maxContacts = students[i];
                }
            }
        }
        return maxContacts;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(String.format("{\"fakultet\":\"%s\", \"studenti\":[", name));
        for (int i = 0; i < students.length - 1; i++) {
            sb.append(students[i].toString());
            sb.append(", ");
        }
        sb.append(students[students.length - 1].toString());
        sb.append("]}");
        return sb.toString();
    }
}
