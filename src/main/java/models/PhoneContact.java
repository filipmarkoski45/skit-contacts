package models;

public class PhoneContact extends Contact {
    private String phone;

    public PhoneContact(String date, String phone) {
        super(date);
        this.phone = phone;
    }

    public PhoneContact(PhoneContact phoneContact) {
        super(phoneContact);
        this.phone = phoneContact.phone;
    }

    public String getPhone() {
        return phone;
    }

    public Operator getOperator() {
        char prefix = phone.charAt(2);
        if (prefix == '0' || prefix == '1' || prefix == '2') {
            return Operator.TMOBILE;
        } else if (prefix == '5' || prefix == '6') {
            return Operator.ONE;
        } else {
            return Operator.VIP;
        }
    }

    public String getType() {
        return "Phone";
    }
}
