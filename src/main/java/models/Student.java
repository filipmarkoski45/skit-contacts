package models;

public class Student {
    private String firstname;
    private String lastname;
    private String city;
    private int age;
    private long index;
    private Contact[] contacts;
    private int emailContacts;
    private int phoneContacts;

    public Student(String firstname, String lastname, String city, int age, long index) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.city = city;
        this.age = age;
        this.index = index;
        this.contacts = new Contact[0];
        this.emailContacts = 0;
        this.phoneContacts = 0;
    }

    public Student(Student student) {
        firstname = student.firstname;
        lastname = student.lastname;
        city = student.city;
        age = student.age;
        index = student.index;
        emailContacts = student.emailContacts;
        phoneContacts = student.phoneContacts;
        contacts = new Contact[student.contacts.length];
        for (int i = 0; i < contacts.length; i++) {
            if (student.contacts[i].getType().equals("Phone")) {
                contacts[i] = new PhoneContact((PhoneContact) student.contacts[i]);
            } else if (student.contacts[i].getType().equals("Email")) {
                contacts[i] = new EmailContact((EmailContact) student.contacts[i]);
            }
        }
    }

    private void increaseContacts() {
        Contact temp[] = new Contact[contacts.length];
        System.arraycopy(contacts, 0, temp, 0, contacts.length);
        /* better to just temp = contacts so you don't make a useless copy */
        contacts = new Contact[temp.length + 1];
        System.arraycopy(temp, 0, contacts, 0, temp.length);
    }

    public void addEmailContact(String date, String email) {
        increaseContacts();
        contacts[contacts.length - 1] = new EmailContact(date, email);
        emailContacts++;
    }

    public void addPhoneContact(String date, String phone) {
        increaseContacts();
        contacts[contacts.length - 1] = new PhoneContact(date, phone);
        phoneContacts++;
    }

    public Contact[] getEmailContacts() {
        Contact result[] = new Contact[emailContacts];
        int k = 0;
        for (int i = 0; i < contacts.length; i++) {
            if (contacts[i].getType().equals("Email")) {
                result[k++] = contacts[i];
            }
        }
        return result;
    }

    public Contact[] getPhoneContacts() {
        Contact result[] = new Contact[phoneContacts];
        int k = 0;
        for (int i = 0; i < contacts.length; i++) {
            if (contacts[i].getType().equals("Phone")) {
                result[k++] = contacts[i];
            }
        }
        return result;
    }

    public String getCity() {
        return city;
    }

    public String getFullName() {
        StringBuffer sb = new StringBuffer();
        sb.append(firstname + " " + lastname);
        return sb.toString();
    }

    public long getIndex() {
        return index;
    }

    public int getNumberContacts() {
        return contacts.length;
    }

    public Contact getLatestContact() {
        Contact result = contacts[0];
        for (int i = 1; i < contacts.length; i++) {
            if (contacts[i].isNewerThan(result)) {
                result = contacts[i];
            }
        }
        return result;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(String.format("{"
                + "\"ime\":\"%s\", "
                + "\"prezime\":\"%s\", "
                + "\"vozrast\":%d, "
                + "\"grad\":\"%s\", "
                + "\"indeks\":%d, "
                + "\"telefonskiKontakti\":[", firstname, lastname, age, city, index));
        boolean last = false;
        int counter = 0;
        for (int i = 0; i < contacts.length; i++) {
            if (counter == phoneContacts) {
                break;
            }
            if (contacts[i].getType().equals("Phone")) {
                if (counter == phoneContacts - 1) {
                    last = true;
                }
                sb.append(String.format("\"%s\"", ((PhoneContact) contacts[i]).getPhone()));
                if (!last) {
                    sb.append(", ");
                }
                counter++;
            }
        }
        sb.append("], \"emailKontakti\":[");

        counter = 0;
        last = false;
        for (int i = 0; i < contacts.length; i++) {
            if (counter == emailContacts) {
                break;
            }
            if (contacts[i].getType().equals("Email")) {
                if (counter == emailContacts - 1) {
                    last = true;
                }
                sb.append(String.format("\"%s\"", ((EmailContact) contacts[i]).getEmail()));
                if (!last) {
                    sb.append(", ");
                }
                counter++;
            }
        }
        sb.append("]}");
        return sb.toString();
    }
}

