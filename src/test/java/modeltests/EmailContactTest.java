package modeltests;

import models.EmailContact;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class EmailContactTest {

    private String date;
    private String email;
    private EmailContact emailContact;

    @BeforeEach
    void setUp() throws Exception {
        this.date = "2019-01-01";
        this.email = "filip.markoski45@gmail.com";
        this.emailContact = new EmailContact(date, email);
    }

    @AfterEach
    void tearDown() throws Exception {
        System.gc();
    }

    /*
     * The constructors don't have any atypical logic,
     * thus they're a better off let untested
     * */
    @Test
    void testEmailContactStringString() {
        this.emailContact = new EmailContact(date, email);
    }

    @Test
    void testEmailContactEmailContact() {
        this.emailContact = new EmailContact(this.emailContact);
    }

    /*
     * Getter methods should be ignored as they test
     * the compiler more so that the application
     * */
    @Test
    void testGetEmail() {
        assertEquals(this.email, this.emailContact.getEmail());
    }

    @Test
    void testGetType() {
        assertEquals("Email", this.emailContact.getType());
    }

}
