package modeltests;
// import com.jparams.verifier.tostring.ToStringVerifier;

import models.Contact;
import models.EmailContact;
import models.Student;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


/**
 * @author Lenovo
 */
public class StudentTest {
    private String date2019;
    private String date2018;
    private String date2017;
    private String email;
    private String phone;

    private Student filipMarkoski;

    /**
     * @throws Exception
     */
    @BeforeEach
    public void setUp() throws Exception {
        this.date2019 = "2019-01-01";
        this.date2018 = "2018-01-01";
        this.date2017 = "2017-01-01";

        this.email = "filip.markoski45@gmail.com";
        this.phone = "071/293-064";

        // String firstname, String lastname, String city, int age, long index
        this.filipMarkoski = new Student("Filip", "Markoski", "Skopje", 21, 161528);

    }

    /**
     * @throws Exception
     */
    @AfterEach
    public void tearDown() throws Exception {
        System.gc();
    }

    /* Test methods for constructors */

    // String firstname, String lastname, String city, int age, long index) {
    @Test
    public void testStudentStringStringStringIntLong() {
        this.filipMarkoski = new Student("Filip", "Markoski", "Skopje", 21, 161528);
        assertEquals(161528L, this.filipMarkoski.getIndex());
        assertEquals("Filip Markoski", this.filipMarkoski.getFullName());
        assertEquals("Skopje", this.filipMarkoski.getCity());
        assertEquals(0, this.filipMarkoski.getNumberContacts());
        assertEquals(0, this.filipMarkoski.getEmailContacts().length);
    }

    @Test
    public void testStudentStudent() {
        this.filipMarkoski.addEmailContact(date2019, email);
        this.filipMarkoski.addPhoneContact(date2018, phone);
        this.filipMarkoski = new Student(this.filipMarkoski);
        assertEquals(161528L, this.filipMarkoski.getIndex());
        assertEquals("Filip Markoski", this.filipMarkoski.getFullName());
        assertEquals("Skopje", this.filipMarkoski.getCity());
        assertEquals(2, this.filipMarkoski.getNumberContacts());
        assertEquals(1, this.filipMarkoski.getEmailContacts().length);
    }

    /* Test methods for getters */
    @Test
    public void testGetCity() {
        assertEquals("Skopje", this.filipMarkoski.getCity());
    }

    @Test
    public void testGetFullName() {
        assertEquals("Filip Markoski", this.filipMarkoski.getFullName());
    }

    @Test
    public void testGetIndex() {
        assertEquals(161528L, this.filipMarkoski.getIndex());
    }

    @Test
    public void testGetNumberContacts() {
        assertEquals(0, this.filipMarkoski.getNumberContacts());
    }


    /**
     * Test method for {@link Student#addEmailContact(String, String)}.
     */

    /**
     * Method: addEmailContact()
     * Approach: Interface-based
     * C1	`date2019` is not null
     * C2	`date2019` is not empty
     * C3	`email` is not null
     * C4	`email` is not empty
     * <p>
     * FTTT -> FFTT,
     * TTFT -> TTFF
     * <p>
     * Test Requirements: {TTTT, FFTT, TFTT, TTFF, TTTF}
     */
    @Test
    public void testAddEmailContact_iTTTT() {
        String date = "2019-01-01";
        String email = "filip.markoski45@gmail.com";
        filipMarkoski.addEmailContact(date, email);
        assertEquals(1, filipMarkoski.getEmailContacts().length);
    }

    @Test
    public void testAddEmailContact_iFFTT() {
        // date2019 is null
        String date = null;
        String email = "filip.markoski45@gmail.com";

        assertThrows(RuntimeException.class, () -> filipMarkoski.addEmailContact(date, email));
    }

    @Test
    public void testAddEmailContact_iTFTT() {
        // date2019 is empty
        String date = "";
        String email = "filip.markoski45@gmail.com";

        assertThrows(RuntimeException.class, () -> filipMarkoski.addEmailContact(date, email));
    }

    @Test
    public void testAddEmailContact_iTTFF() {
        // email is null
        String date = "2019-01-01";
        String email = null;

        assertThrows(RuntimeException.class, () -> filipMarkoski.addEmailContact(date, email));
    }

    @Test
    public void testAddEmailContact_iTTTF() {
        // email is empty
        String date = "2019-01-01";
        String email = "";

        assertThrows(RuntimeException.class, () -> filipMarkoski.addEmailContact(date, email));
    }

    /**
     * Method: addEmailContact()
     * Approach: Functionality-based
     * C1	`date2019` has valid form
     * C2	`email` has valid form
     * <p>
     * Test Requirements: {TT, FT, TF}
     */
    @Test
    public void testAddEmailContact_fTT() {
        String date = "2019-01-01";
        String email = "filip.markoski45@gmail.com";
        filipMarkoski.addEmailContact(date, email);
        assertEquals(1, filipMarkoski.getEmailContacts().length);
    }

    @Test
    public void testAddEmailContact_fFT() {
        String date = "01-01-2019";
        String email = "filip.markoski45@gmail.com";
        assertThrows(RuntimeException.class, () -> filipMarkoski.addEmailContact(date, email));
    }

    @Test
    public void testAddEmailContact_fTF() {
        String date = "2019-01-01";
        String email = "filip^markoski45gmailcom";
        assertThrows(RuntimeException.class, () -> filipMarkoski.addEmailContact(date, email));
    }

    /**
     * Test method for {@link Student#addPhoneContact(String, String)}.
     */

    /**
     * Method: AddPhoneContact()
     * Approach: Interface-based
     * C1	`date2019` is not null
     * C2	`date2019` is not empty
     * C3	`phone` is not null
     * C4	`phone` is not empty
     * <p>
     * FTTT -> FFTT,
     * TTFT -> TTFF
     * <p>
     * Test Requirements: {TTTT, FFTT, TFTT, TTFF, TTTF}
     */
    @Test
    public void testAddPhoneContact_iTTTT() {
        String date = "2019-01-01";
        String phone = "071/293-064";
        filipMarkoski.addPhoneContact(date, phone);
        assertEquals(1, filipMarkoski.getPhoneContacts().length);
    }

    @Test
    public void testAddPhoneContact_iFFTT() {
        // date2019 is null
        String date = null;
        String phone = "071/293-064";

        assertThrows(RuntimeException.class, () -> filipMarkoski.addPhoneContact(date, phone));
    }

    @Test
    public void testAddPhoneContact_iTFTT() {
        // date2019 is empty
        String date = "";
        String phone = "071/293-064";

        assertThrows(RuntimeException.class, () -> filipMarkoski.addPhoneContact(date, phone));
    }

    @Test
    public void testAddPhoneContact_iTTFF() {
        // phone is null
        String date = "2019-01-01";
        String phone = null;

        assertThrows(RuntimeException.class, () -> filipMarkoski.addPhoneContact(date, phone));
    }

    @Test
    public void testAddPhoneContact_iTTTF() {
        // phone is empty
        String date = "2019-01-01";
        String phone = "";

        assertThrows(RuntimeException.class, () -> filipMarkoski.addPhoneContact(date, phone));
    }

    /**
     * Method: AddPhoneContact()
     * Approach: Functionality-based
     * C1	`date2019` has valid form
     * C2	`phone` has valid form
     * <p>
     * Test Requirements: {TT, FT, TF}
     */
    @Test
    public void testAddPhoneContact_fTT() {
        String date = "2019-01-01";
        String phone = "071/293-064";
        filipMarkoski.addPhoneContact(date, phone);
        assertEquals(1, filipMarkoski.getPhoneContacts().length);
    }

    @Test
    public void testAddPhoneContact_fFT() {
        String date = "01-01-2019";
        String phone = "071/293-064";
        assertThrows(RuntimeException.class, () -> filipMarkoski.addPhoneContact(date, phone));
    }

    @Test
    public void testAddPhoneContact_fTF() {
        String date = "2019-01-01";
        String phone = "005123321";
        assertThrows(RuntimeException.class, () -> filipMarkoski.addPhoneContact(date, phone));
    }

    /**
     * Test method for {@link Student#getEmailContacts()}.
     */
    /**
     * Method: Student.getEmailContacts()
     * Approach: Interface-based
     * C1	`contacts` is not null
     * C2	`contacts` is not empty
     * C3	`contacts` has length greater than 1
     * C4	`contacts` does not have null-date2019 elements
     * Test Requirements: {TTTT, FTTT, TFTT, TTFT, TTTF}
     * FTTT -> FFFF,
     * TFTT -> TFFF
     */
    @Test
    public void testGetEmailContacts_iTTTT() {
        filipMarkoski.addEmailContact(date2019, email);
        filipMarkoski.addEmailContact(date2019, email);
        assertTrue(filipMarkoski.getEmailContacts().length > 1);
        for (Contact contact : filipMarkoski.getEmailContacts()) assertNotNull(contact);
    }

    @Test
    public void testGetEmailContacts_iFFFF() {
        // upon object construction, the `contacts` array shouldn't be able to be null
        assertNotNull(filipMarkoski.getEmailContacts());
    }

    @Test
    public void testGetEmailContacts_iTFFF() {
        // empty contacts
        assertEquals(0, filipMarkoski.getEmailContacts().length);
    }

    @Test
    public void testGetEmailContacts_iTTFT() {
        // contacts length > 1
        filipMarkoski.addEmailContact(date2019, email);
        filipMarkoski.addEmailContact(date2019, email);
        assertTrue(filipMarkoski.getEmailContacts().length > 1);
    }

    @Test
    public void testGetEmailContacts_iTTTF() {
        // contacts with null dates
        filipMarkoski.addEmailContact(null, email);
        filipMarkoski.addEmailContact(null, email);
        /*
         * Dates shouldn't ever be null because they're crucially important in other methods
         * Discussion is needed about whether Student should be a model class or something more */
        assertThrows(RuntimeException.class, filipMarkoski::getEmailContacts);
    }

    /*
     * Method: Student.getEmailContacts()
     * Approach: Functionality-based
     *
     * Non-applicable
     * */


    /*
     * Method: Student.getEmailContacts()
     * Approach: Prime Path Coverage
     * 1.	`contacts` array has a `Contact` object with type â€œEmailâ€� and another object which doesnâ€™t.
     * 2.	`contacts` array has two `Contact` objects not of type â€œEmailâ€�
     * 3.	`contacts` array is empty
     * 4.	`contacts` array contains only one object of type â€œEmailâ€�
     * */
    @Test
    public void testGetEmailContacts_g1() {
        // 1.	`contacts` array has a `Contact` object with type â€œEmailâ€� and another object which doesnâ€™t.
        filipMarkoski.addEmailContact(date2019, email);
        filipMarkoski.addPhoneContact(date2018, phone);
        Contact[] emailContacts = filipMarkoski.getEmailContacts();
        assertEquals(1, emailContacts.length);
        Contact emailContact = emailContacts[0];
        assertEquals("Email", emailContact.getType());

        /* Fault: there are no getters, we cannot confirm
         * if contact being sent is the same one being returned */
    }

    @Test
    public void testGetEmailContacts_g2() {
        // 2.	`contacts` array has two `Contact` objects not of type â€œEmailâ€�
        filipMarkoski.addPhoneContact(date2019, phone);
        filipMarkoski.addPhoneContact(date2018, phone);
        Contact[] emailContacts = filipMarkoski.getEmailContacts();
        assertEquals(0, emailContacts.length);
    }


    @Test
    public void testGetEmailContacts_g3() {
        // 3.	`contacts` array is empty
        Contact[] emailContacts = filipMarkoski.getEmailContacts();
        assertEquals(0, emailContacts.length);
    }

    @Test
    public void testGetEmailContacts_g4() {
        // 4.	`contacts` array contains only one object of type â€œEmailâ€�
        filipMarkoski.addEmailContact(date2019, email);
        Contact[] emailContacts = filipMarkoski.getEmailContacts();
        assertEquals(1, emailContacts.length);
        Contact emailContact = emailContacts[0];
        assertEquals("Email", emailContact.getType());
    }

    /*
      Test method for {@link Student#getPhoneContacts()}.
     */

    /**
     * Method: Student.getPhoneContacts()
     * Approach: Interface-based
     * C1	`contacts` is not null
     * C2	`contacts` is not empty
     * C3	`contacts` has length greater than 1
     * C4	`contacts` does not have null-date2019 elements
     * Test Requirements: {TTTT, FTTT, TFTT, TTFT, TTTF}
     * FTTT -> FFFF,
     * TFTT -> TFFF
     */
    @Test
    public void testGetPhoneContacts_iTTTT() {
        filipMarkoski.addPhoneContact(date2019, phone);
        filipMarkoski.addPhoneContact(date2019, phone);
        assertTrue(filipMarkoski.getPhoneContacts().length > 1);
        for (Contact contact : filipMarkoski.getPhoneContacts()) assertNotNull(contact);
    }

    @Test
    public void testGetPhoneContacts_iFFFF() {
        // upon object construction, the `contacts` array shouldn't be able to be null
        assertNotNull(filipMarkoski.getPhoneContacts());
    }

    @Test
    public void testGetPhoneContacts_iTFFF() {
        // empty contacts
        assertEquals(0, filipMarkoski.getPhoneContacts().length);
    }

    @Test
    public void testGetPhoneContacts_iTTFT() {
        // contacts length > 1
        filipMarkoski.addPhoneContact(date2019, phone);
        filipMarkoski.addPhoneContact(date2019, phone);
        assertTrue(filipMarkoski.getPhoneContacts().length > 1);
    }

    @Test
    public void testGetPhoneContacts_iTTTF() {
        // contacts with null dates
        filipMarkoski.addPhoneContact(null, phone);
        filipMarkoski.addPhoneContact(null, phone);
        /*
         * Dates shouldn't ever be null because they're crucially important in other methods
         * Discussion is needed about whether Student should be a model class or something more */
        assertThrows(RuntimeException.class, filipMarkoski::getEmailContacts);
    }

    /*
     * Method: Student.getPhoneContacts()
     * Approach: Functionality-based
     *
     * Non-applicable
     * */


    /*
     * Method: Student.getPhoneContacts()
     * Approach: Prime Path Coverage
     * 1.	`contacts` array has a `Contact` object with type â€œPhoneâ€� and another object which doesnâ€™t.
     * 2.	`contacts` array has two `Contact` objects not of type â€œPhoneâ€�
     * 3.	`contacts` array is empty
     * 4.	`contacts` array contains only one object of type â€œPhoneâ€�
     * */
    @Test
    public void testGetPhoneContacts_g1() {
        // 1.	`contacts` array has a `Contact` object with type "Phone" and another object which doesnâ€™t.
        filipMarkoski.addPhoneContact(date2019, phone);
        filipMarkoski.addEmailContact(date2018, email);
        Contact[] phoneContacts = filipMarkoski.getPhoneContacts();
        assertEquals(1, phoneContacts.length);
        Contact phoneContact = phoneContacts[0];
        assertEquals("Phone", phoneContact.getType());

        /* Fault: there are no getters, we cannot confirm
         * if contact being sent is the same one being returned */
    }

    @Test
    public void testGetPhoneContacts_g2() {
        // 2.	`contacts` array has two `Contact` objects not of type "Phone"
        filipMarkoski.addEmailContact(date2019, email);
        filipMarkoski.addEmailContact(date2018, email);
        Contact[] phoneContacts = filipMarkoski.getPhoneContacts();
        assertEquals(0, phoneContacts.length);
    }


    @Test
    public void testGetPhoneContacts_g3() {
        // 3.	`contacts` array is empty
        Contact[] phoneContacts = filipMarkoski.getPhoneContacts();
        assertEquals(0, phoneContacts.length);
    }

    @Test
    public void testGetPhoneContacts_g4() {
        // 4.	`contacts` array contains only one object of type "Phone"
        filipMarkoski.addPhoneContact(date2019, phone);
        Contact[] phoneContacts = filipMarkoski.getPhoneContacts();
        assertEquals(1, phoneContacts.length);
        Contact phoneContact = phoneContacts[0];
        assertEquals("Phone", phoneContact.getType());
    }


    /**
     * Test method for {@link Student#getLatestContact()}.
     */
    /**
     * Method: Student.getLatestContact()
     * Approach: Interface-based
     * C1	`contacts` is not null
     * C2	`contacts` is not empty
     * C3	`contacts` has length greater than 1
     * C4	`contacts` does not have null-date elements
     * Test Requirements: {TTTT, FTTT, TFTT, TTFT, TTTF}
     * FTTT -> FFFF,
     * TFTT -> TFFF
     */
    @Test
    public void testGetLatestContact_iTTTT() {
        filipMarkoski.addPhoneContact(date2019, phone);
        filipMarkoski.addPhoneContact(date2018, phone);
        EmailContact emailContact = new EmailContact(date2017, email);
        assertTrue(filipMarkoski.getLatestContact().isNewerThan(emailContact));
    }

    @Test
    public void testGetLatestContact_iFFFF() {
        // upon object construction, the `contacts` array is constructed. Not null, just empty

        /* ArrayIndexOutOfBoundsException.class is the exception which shall be received,
         * but the method should throw a more descriptive custom exception,
         * e.g. EmptyContactsException */
        assertThrows(RuntimeException.class, filipMarkoski::getLatestContact);
    }

    @Test
    public void testGetLatestContact_iTFFF() {
        // empty contacts

        // a more proper exception should be thrown
        assertThrows(ArrayIndexOutOfBoundsException.class, filipMarkoski::getLatestContact);
    }

    @Test
    public void testGetLatestContact_iTTFT() {
        // contacts length <= 1
        filipMarkoski.addPhoneContact(date2019, phone);
        assertEquals("Phone", filipMarkoski.getLatestContact().getType());
    }

    @Test
    public void testGetLatestContact_iTTTF() {
        // contacts with null dates
        filipMarkoski.addPhoneContact(null, phone);
        filipMarkoski.addPhoneContact(null, phone);
        /*
         * Dates shouldn't ever be null because they're crucially important in other methods
         * Discussion is needed about whether Student should be a model class or something more */
        assertThrows(RuntimeException.class, filipMarkoski::getLatestContact);
    }


    /**
     * Method: Student.getLatestContact()
     * Approach: Prime Path Coverage
     * 1.	`contacts` = [Contact{date=2017-01-01}, Contact{date=â€�2018-01-01â€�}, Contact{date=â€�2019-01-01â€�}]
     * 2.	`contacts` = [Contact{date=2019-01-01}, Contact{date=â€�2018-01-01â€�}, Contact{date=â€�2017-01-01â€�}] (reversed)
     * 3.	`contacts` = []
     * 4.	`contacts` = [Contact{date=2019-01-01}, Contact{date=â€�2018-01-01â€�}]
     */
    @Test
    public void testGetLatestContact_g1() {
        // 1.	`contacts` = [Contact{date=2017-01-01}, Contact{date=â€�2018-01-01â€�}, Contact{date=â€�2019-01-01â€�}]
        filipMarkoski.addEmailContact(date2017, email);
        filipMarkoski.addEmailContact(date2018, email);
        filipMarkoski.addEmailContact(date2019, email);
        Contact emailContact = filipMarkoski.getEmailContacts()[2];
        assertEquals(emailContact, filipMarkoski.getLatestContact());
    }

    @Test
    public void testGetLatestContact_g2() {
        // 2.	`contacts` = [Contact{date=2019-01-01}, Contact{date=â€�2018-01-01â€�}, Contact{date=â€�2017-01-01â€�}] (reversed)
        filipMarkoski.addEmailContact(date2019, email);
        filipMarkoski.addEmailContact(date2018, email);
        filipMarkoski.addEmailContact(date2017, email);
        Contact emailContact = filipMarkoski.getEmailContacts()[0];
        assertEquals(emailContact, filipMarkoski.getLatestContact());
    }

    @Test
    public void testGetLatestContact_g3() {
        // 3.	`contacts` = []

        // a more proper exception should be thrown
        assertThrows(ArrayIndexOutOfBoundsException.class, filipMarkoski::getLatestContact);
    }

    @Test
    public void testGetLatestContact_g4() {
        // 4.	`contacts` = [Contact{date=2019-01-01}, Contact{date=â€�2018-01-01â€�}]
        filipMarkoski.addEmailContact(date2019, email);
        filipMarkoski.addEmailContact(date2018, email);
        Contact emailContact = filipMarkoski.getEmailContacts()[0];
        assertEquals(emailContact, filipMarkoski.getLatestContact());
    }

    /**
     * Test method for {@link Student#toString()}.
     */
    @Test
    public void testToString() {
        // TODO: Student.toString formatting test
        // ToStringVerifier.forClass(Student.class).verify();
    }

    /**
     * Method: Student.toString()
     * Approach: Edge-pair Coverage
     * <p>
     * 1.	contacts=[]
     * 2.	contacts=[EmailContact(), PhoneContact()]
     * 3.	contacts=[PhoneContact()]
     * 4.	contacts=[PhoneContact(), PhoneContact()]
     * 5.	contacts=[EmailContact()]
     * 6.	contacts=[EmailContact(), EmailContact()]
     */
    @Test
    public void testToString_g1() {
        // 1. contacts=[]
        String expected = "{\"ime\":\"Filip\", \"prezime\":\"Markoski\", \"vozrast\":21, \"grad\":\"Skopje\", \"indeks\":161528, \"telefonskiKontakti\":[], \"emailKontakti\":[]}";
        assertEquals(expected, filipMarkoski.toString());
    }

    @Test
    public void testToString_g2() {
        // 2.	contacts=[EmailContact(), PhoneContact()]
        String expected = "{\"ime\":\"Filip\", \"prezime\":\"Markoski\", \"vozrast\":21, \"grad\":\"Skopje\", \"indeks\":161528, \"telefonskiKontakti\":[\"071/293-064\"], \"emailKontakti\":[\"filip.markoski45@gmail.com\"]}";

        filipMarkoski.addEmailContact(date2019, email);
        filipMarkoski.addPhoneContact(date2018, phone);
        assertEquals(expected, filipMarkoski.toString());
    }

    @Test
    public void testToString_g3() {
        // 3.	contacts=[PhoneContact()]
        String expected = "{\"ime\":\"Filip\", \"prezime\":\"Markoski\", \"vozrast\":21, \"grad\":\"Skopje\", \"indeks\":161528, \"telefonskiKontakti\":[\"071/293-064\"], \"emailKontakti\":[]}";

        filipMarkoski.addPhoneContact(date2019, phone);
        assertEquals(expected, filipMarkoski.toString());
    }

    @Test
    public void testToString_g4() {
        // 4.	contacts=[PhoneContact(), PhoneContact()]
        String expected = "{\"ime\":\"Filip\", \"prezime\":\"Markoski\", \"vozrast\":21, \"grad\":\"Skopje\", \"indeks\":161528, \"telefonskiKontakti\":[\"071/293-064\", \"071/293-064\"], \"emailKontakti\":[]}";

        filipMarkoski.addPhoneContact(date2019, phone);
        filipMarkoski.addPhoneContact(date2018, phone);
        assertEquals(expected, filipMarkoski.toString());
    }

    @Test
    public void testToString_g5() {
        // 5.	contacts=[EmailContact()]
        String expected = "{\"ime\":\"Filip\", \"prezime\":\"Markoski\", \"vozrast\":21, \"grad\":\"Skopje\", \"indeks\":161528, \"telefonskiKontakti\":[], \"emailKontakti\":[\"filip.markoski45@gmail.com\"]}";

        filipMarkoski.addEmailContact(date2019, email);
        assertEquals(expected, filipMarkoski.toString());
    }


    @Test
    public void testToString_g6() {
        // 6.	contacts=[EmailContact(), EmailContact()]
        String expected = "{\"ime\":\"Filip\", \"prezime\":\"Markoski\", \"vozrast\":21, \"grad\":\"Skopje\", \"indeks\":161528, \"telefonskiKontakti\":[], \"emailKontakti\":[\"filip.markoski45@gmail.com\", \"filip.markoski45@gmail.com\"]}";

        filipMarkoski.addEmailContact(date2019, email);
        filipMarkoski.addEmailContact(date2018, email);
        assertEquals(expected, filipMarkoski.toString());
    }

}