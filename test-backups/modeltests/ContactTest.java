package modeltests;

import models.Contact;
import models.EmailContact;
import models.PhoneContact;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.text.ParseException;

import static org.junit.jupiter.api.Assertions.*;

public class ContactTest {

    private String date;
    private String email;
    private Contact contact;

    @BeforeEach
    public void setUp() throws Exception {
        this.date = "2019-01-01";
        this.email = "filip.markoski45@gmail.com";

        this.contact = new EmailContact(date, email);
    }

    @AfterEach
    public void tearDown() throws Exception {
    }

    @Test
    void testContactString() {
        this.contact = new Contact(date) {
            @Override
            public String getType() {
                return "QR";
            }
        };
    }

    @Test
    void testContactContact() {
        this.contact = new Contact(contact) {
            @Override
            public String getType() {
                return "Email";
            }
        };
    }

    @Test
    void testGetType() {
        assertEquals("Email", this.contact.getType());
    }

    /**
     * Method: isNewerThan()
     * Approach: Prime Path Coverage
     * 1.	Contact with a non-null date field containing a date of the format “yyyy-MM-dd”
     * 2.	Contact with a null date field or a date not of the “yyyy-MM-dd” format
     */
    @Test
    void testIsNewerThan_g1() {
        // Contact with a non-null date field containing a date of the format “yyyy-MM-dd”
        String date = "2012-04-10";
        String email = "sanja@ef.ukim.mk";
        Contact emailContact = new EmailContact(date, email);

        date = "2019-03-05";
        String phone = "070/744-235";
        Contact phoneContact = new PhoneContact(date, phone);

        assertFalse(emailContact.isNewerThan(phoneContact));
    }

    @Test
    void testIsNewerThan_g2() {
        // Contact with a non-null date field containing a date of the format “yyyy-MM-dd”
        String date = "2012-04-10";
        String email = "sanja@ef.ukim.mk";
        Contact emailContact = new EmailContact(date, email);

        date = "20190305";
        String phone = "070/744-235";
        Contact phoneContact = new PhoneContact(date, phone);

        assertThrows(RuntimeException.class, () -> emailContact.isNewerThan(phoneContact));
    }

    /**
     * Method: isNewerThan()
     * Approach: Interface-based
     * C1	c is not null
     * C2	c is not empty
     * C3	c has a valid date
     * Test Requirements: TTT, FFF, TFF, TTF
     */
    @Test
    void testIsNewerThan_iTTT() {
        String date = "2012-04-10";
        String email = "sanja@ef.ukim.mk";
        Contact emailContact = new EmailContact(date, email);

        date = "2010-03-05";
        String phone = "070/744-235";
        Contact phoneContact = new PhoneContact(date, phone);

        Boolean expected = true;
        Boolean received = emailContact.isNewerThan(phoneContact);

        assertEquals(expected, received);
    }

    @Test
    void testIsNewerThan_iFFF() {
        // The input contact is null
        // received: NullPointerException.class
        String date = "2012-04-10";
        String email = "sanja@ef.ukim.mk";
        Contact emailContact = new EmailContact(date, email);

        date = "2010-03-05";
        String phone = "070/744-235";
        Contact phoneContact = null;

        /*
         * The method should be robust in dealing with the various possible inputs,
         * and should be able to throw an IllegalArgumentException.class with a proper message
         * appropriately warning the user of what kind of input is acceptable
         *
         * Testing of whether a NullPointerException.class was thrown tests the compiler
         * more than it tests the method
         * */
        assertThrows(IllegalArgumentException.class, () -> emailContact.isNewerThan(phoneContact));
    }

    @Test
    void testIsNewerThan_iTFF() {
        // The input contact's contents are null, i.e. c.date = null
        String date = "2012-04-10";
        String email = "sanja@ef.ukim.mk";
        Contact emailContact = new EmailContact(date, email);

        date = null;
        String phone = "070/744-235";
        Contact phoneContact = new PhoneContact(date, phone);

        /* Proper input validation needs to throw the appropriate exception */
        assertThrows(IllegalArgumentException.class, () -> emailContact.isNewerThan(phoneContact));
    }

    @Test
    void testIsNewerThan_iTTF() {
        String date = "2012-04-10";
        String email = "sanja@ef.ukim.mk";
        Contact emailContact = new EmailContact(date, email);

        date = "03-05-2019";
        String phone = "070/744-235";
        Contact phoneContact = new PhoneContact(date, phone);

        /* The method should be robust to various different date formats
         * or it should alert with a proper exception which are the correct formats
         * or if it has encountered a date format which it cannot parse it should throw it */
        assertThrows(ParseException.class, () -> emailContact.isNewerThan(phoneContact));
    }

    /**
     * isNewerThan() [Functionality-based]
     * 1: c is newer
     * 2: c is not temporally equal
     * Test Requirements: TT, FT, FF
     */
    @Test
    void testIsNewerThan_fTT() {
        // c is newer and not equal

        String date = "2012-04-10";
        String email = "sanja@ef.ukim.mk";
        Contact emailContact = new EmailContact(date, email);

        date = "2013-03-05";
        String phone = "070/744-235";
        Contact phoneContact = new PhoneContact(date, phone);

        Boolean expected = false;
        Boolean received = emailContact.isNewerThan(phoneContact);

        assertEquals(expected, received);
    }

    @Test
    void testIsNewerThan_fFT() {
        // c is older

        String date = "2012-04-10";
        String email = "sanja@ef.ukim.mk";
        Contact emailContact = new EmailContact(date, email);

        date = "2011-03-05";
        String phone = "070/744-235";
        Contact phoneContact = new PhoneContact(date, phone);

        Boolean expected = true;
        Boolean received = emailContact.isNewerThan(phoneContact);

        assertEquals(expected, received);
    }

    @Test
    void testIsNewerThan_fFF() {
        // c is temporally equal

        String date = "2012-04-10";
        String email = "sanja@ef.ukim.mk";
        Contact emailContact = new EmailContact(date, email);

        date = "2012-04-10";
        String phone = "070/744-235";
        Contact phoneContact = new PhoneContact(date, phone);

        Boolean expected = false;
        Boolean received = emailContact.isNewerThan(phoneContact);

        /* We would expect for the method to return false because the question is
         * whether it is newer, and since it isn't, we should expect it to return false
         * Thus, although this method is not the most informative, it is technically correct
         * in its evaluation */
        assertEquals(expected, received);
    }


}
