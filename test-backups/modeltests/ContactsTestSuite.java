package modeltests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        ContactTest.class, EmailContactTest.class, PhoneContactTest.class,
        StudentTest.class, FacultyTest.class
})
public class ContactsTestSuite {
}
