package modeltests;

import models.Faculty;
import models.Student;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.Assert.*;


@ExtendWith(MockitoExtension.class)
// @RunWith(JUnitPlatform.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class FacultyMockitoTest {

    @Mock
    Student filipMarkoski;

    @InjectMocks
    Faculty faculty = new Faculty("FCSE", new Student[]{});

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @AfterEach
    public void tearDown() throws Exception {
        System.gc();
    }


    @Test
    public void test() {
        Mockito.lenient().when(filipMarkoski.getIndex()).thenReturn(161528L);

        this.faculty = Mockito.mock(Faculty.class);
        Mockito.lenient().when(faculty.getStudent(161528L)).thenReturn(filipMarkoski);
        assertEquals(filipMarkoski.getIndex(), faculty.getStudent(161528L).getIndex());
    }

}
