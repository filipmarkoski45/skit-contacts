package modeltests;

import models.Faculty;
import models.Student;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FacultyTest {
    private String date2019;
    private String date2018;
    private String date2017;
    private String email;
    private String phone;

    private List<Student> students;
    private Student filipMarkoski;
    private Student milaVasileska;
    private Student emilTrajanov;
    private Faculty faculty;

    /**
     * Method: Faculty.countStudentsFromCity(String)
     * Approach: Prime Path Coverage
     * <p>
     * 1.	city=”Skopje”, students=[Student(Skopje), Student(Skopje)]
     * 2.	city=”Berlin”, students=[Student(Skopje), Student(Skopje)]
     * 3.	city=”Berlin”, students=[]
     * 4.	city=”Berlin”, students=[Student(Skopje)]
     */
    public static List<Object[]> countStudentsFromCityTRs() {
        Student filipMarkoski = new Student("Filip", "Markoski", "Skopje", 21, 161528);
        Student milaVasileska = new Student("Mila", "Vasileska", "Skopje", 19, 17246);

        return Arrays.asList(new Object[][]{
                {new Faculty("FCSE", new Student[]{filipMarkoski, milaVasileska}), "Skopje", 2},
                {new Faculty("FCSE", new Student[]{filipMarkoski, milaVasileska}), "Berlin", 0},
                {new Faculty("FCSE", new Student[]{}), "Berlin", 0},
                {new Faculty("FCSE", new Student[]{filipMarkoski}), "Berlin", 0},
        });
    }

    /**
     * Method: Faculty.getStudent(long)
     * Approach: Prime Path Coverage
     * <p>
     * 1.	index=161528, students=[]
     * 2.	index=161528, students=[Student(index=161528)]
     */
    public static List<Object[]> getStudentTRs() {
        Student filipMarkoski = new Student("Filip", "Markoski", "Skopje", 21, 161528);
        Student milaVasileska = new Student("Mila", "Vasileska", "Skopje", 19, 17246);

        return Arrays.asList(new Object[][]{
                {new Faculty("FCSE", new Student[]{filipMarkoski, milaVasileska}), filipMarkoski.getIndex(), filipMarkoski},
                {new Faculty("FCSE", new Student[]{filipMarkoski}), filipMarkoski.getIndex(), filipMarkoski},
        });
    }

    /**
     * Method: Faculty.getStudentWithMostContacts()
     * Approach: Logic Coverage (Active)
     * <p>
     * 1.	3: students=[Student(contacts.length = 2, index=111111), Student(contacts.length = 1, index = 222222)]
     * 2.	5: students=[Student(contacts.length = 1, index=222222), Student(contacts.length = 1, index = 111111)]
     * 3.	6: students=[Student(contacts.length = 1, index=111111), Student(contacts.length = 1, index = 111111)]
     * 4.	7: students=[Student(contacts.length = 0, index=111111), Student(contacts.length = 1, index = 222222)]
     */
    public static List<Object[]> getStudentWithMostContactsTRs() {
        String date2019 = "2019-01-01";
        String email = "filip.markoski45@gmail.com";

        Student s2_111111 = new Student("Filip", "Markoski", "s2_111111", 21, 111111);
        Student s1_111111 = new Student("Filip", "Markoski", "s1_111111", 21, 111111);
        Student s0_111111 = new Student("Filip", "Markoski", "s0_111111", 21, 111111);
        Student s0_222222 = new Student("Mila", "Vasileska", "s0_222222", 19, 222222);
        Student s1_222222 = new Student("Mila", "Vasileska", "s1_222222", 19, 222222);

        s2_111111.addEmailContact(date2019, email);
        s2_111111.addEmailContact(date2019, email);
        s1_111111.addEmailContact(date2019, email);

        s1_222222.addEmailContact(date2019, email);

        // Pit Testing:
        Student s0_0_male = new Student("Filip", "Markoski", "s0_0", 21, 0);
        Student s0_0_female = new Student("Mila", "Vasileska", "s0_0", 19, 0);

        return Arrays.asList(new Object[][]{
                {new Faculty("FCSE", new Student[]{s2_111111, s1_222222}), s2_111111},
                {new Faculty("FCSE", new Student[]{s1_222222, s1_111111}), s1_222222},
                {new Faculty("FCSE", new Student[]{s1_111111, s1_111111}), s1_111111}, // equals() needed
                {new Faculty("FCSE", new Student[]{s0_111111, s1_222222}), s1_222222},

                // Logic Coverage (Inactive):
                // 2.	4: students=[Student(contacts.length = 0, index=222222), Student(contacts.length = 1, index = 111111)]
                {new Faculty("FCSE", new Student[]{s0_222222, s1_111111}), s1_111111},

                // me:
                {new Faculty("FCSE", new Student[]{s1_111111, s1_222222}), s1_222222},

                // Pit Testing:
                // both zero contacts with zero indexes
                {new Faculty("FCSE", new Student[]{s0_0_male, s0_0_female}), s0_0_male},
        });
    }

    @BeforeEach
    void setUp() throws Exception {
        this.students = new ArrayList<>();
        this.date2019 = "2019-01-01";
        this.date2018 = "2018-01-01";
        this.date2017 = "2017-01-01";

        this.email = "filip.markoski45@gmail.com";
        this.phone = "071/293-064";

        this.filipMarkoski = new Student("Filip", "Markoski", "Skopje", 21, 161528);
        this.emilTrajanov = new Student("Emil", "Trajanov", "Bitola", 29, 24914336);
        this.milaVasileska = new Student("Mila", "Vasileska", "Skopje", 19, 17246);

        this.filipMarkoski.addEmailContact("2019-01-01", "filip.markoski45@gmail.com");
        this.filipMarkoski.addPhoneContact("2018-01-01", "071/293-064");

        this.students.addAll(Arrays.asList(filipMarkoski, emilTrajanov, milaVasileska));

        Student facultyStudents[] = new Student[this.students.size()];
        facultyStudents = students.toArray(facultyStudents);
        this.faculty = new Faculty("FCSE", facultyStudents);
    }

    @AfterEach
    void tearDown() throws Exception {
        // this.students.clear();
        System.gc();
    }

    /**
     * Method: Faculty.Faculty()
     * Approach: Prime Path Coverage
     * <p>
     * 1.	s=[Student(), Student()]
     * 2.	s=[]
     */
    @Test
    void testFaculty_g1() {
        // 1.	s=[Student(), Student()]
        Faculty faculty = new Faculty("FCSE", new Student[]{this.filipMarkoski, this.milaVasileska});
        assertEquals(this.filipMarkoski.getIndex(), faculty.getStudent(this.filipMarkoski.getIndex()).getIndex());
        assertEquals(this.milaVasileska.getIndex(), faculty.getStudent(this.milaVasileska.getIndex()).getIndex());
        /* Fault: no access to contacts array */
    }

    @Test
    void testFaculty_g2() {
        // 2.   s=[]
        assertDoesNotThrow(() -> {
            new Faculty("FCSE", new Student[]{});
        });
    }

    @ParameterizedTest
    @MethodSource("countStudentsFromCityTRs")
    void testCountStudentsFromCity_g1234(Faculty faculty, String city, Integer expected) {
        assertEquals(expected, faculty.countStudentsFromCity(city));
    }

    /*
     * Method: Faculty.countStudentsFromCity(String)
     * Approach: Interface-based
     * C1	`city` is not null
     * C2	`city` is not empty
     * C3	`city` contains only letters
     * C4	`students` array is not null
     * C5	`students` array is not empty
     * C6	`students` array has length greater than 1
     * C7	`students` array has no null elements
     *
     * Test Requirements: {TTTTTTT, FTTTTTT, TFTTTTT, TTFTTTT, TTTFTTT, TTTTFTT, TTTTTFT, TTTTTTF}
     *                                 *        *                 *        *
     * Test Requirements: {TTTTTTT, FFFTTTT, TFFTTTT, TTFTTTT, TTTFFFT, TTTTFFT, TTTTTFT, TTTTTTF}
     * FTTTTTT -> FFFFTTT,
     * TFTTTTT -> TFFTTTT,
     * TTTFTTT -> TTTFFFT,
     * TTTTFTT -> TTTTFFT
     * */
    @Test
    void testCountStudentsFromCity_iTTTTTTT() {
        // regular city, multiple students
        assertEquals(2, faculty.countStudentsFromCity("Skopje"));
    }

    @Test
    void testCountStudentsFromCity_iFFFTTTT() {
        // null city, multiple students
        // assertThrows(IllegalArgumentException.class, () -> faculty.countStudentsFromCity(null));
    }

    @Test
    void testCountStudentsFromCity_iTFFTTTT() {
        // city = "", multiple students
        // assertThrows(IllegalArgumentException.class, () -> faculty.countStudentsFromCity(""));
    }

    @Test
    void testCountStudentsFromCity_iTTFTTTT() {
        // city = "Sk0pj3", multiple students
        // assertThrows(IllegalArgumentException.class, () -> faculty.countStudentsFromCity("Sk0^j3"));
    }

    @Test
    void testCountStudentsFromCity_iTTTFFFT() {
        // proper city, students is null
        // assertThrows(NullPointerException.class, () -> {            this.faculty = new Faculty("FCSE", null);        });
        // assertThrows(RuntimeException.class, () -> faculty.countStudentsFromCity("Skopje"));
    }

    @Test
    void testCountStudentsFromCity_iTTTTFFT() {
        // proper city, students is empty
        this.faculty = new Faculty("FCSE", new Student[]{});
        assertEquals(0, faculty.countStudentsFromCity("Skopje"));
    }

    @Test
    void testCountStudentsFromCity_iTTTTTFT() {
        // proper city, student length <= 1
        this.faculty = new Faculty("FCSE", new Student[]{this.filipMarkoski});
        assertEquals(1, faculty.countStudentsFromCity("Skopje"));
    }

    @Test
    void testCountStudentsFromCity_iTTTTTTF() {
        // proper city, multiple students, one student is null
        // this.faculty = new Faculty("FCSE", new Student[]{this.filipMarkoski, null});
        // assertEquals(1, faculty.countStudentsFromCity("Skopje"));
    }

    // s@ParameterizedTest
    // @MethodSource("countStudentsFromCityTRs")
    void testGetStudent_g2(Faculty faculty, Long index, Student expected) {
        // Making multiple calls to the same method as a somewhat form of stress testing
        // assertEquals(expected.getIndex(), faculty.getStudent(index).getIndex());
        // assertEquals(expected.getFullName(), faculty.getStudent(index).getFullName());
        // assertEquals(expected.getCity(), faculty.getStudent(index).getCity());
        // assertEquals(expected.getNumberContacts(), faculty.getStudent(index).getNumberContacts());
        // assertEquals(expected.getEmailContacts().length, faculty.getStudent(index).getEmailContacts().length);
    }

    @Test
    void testGetStudent_g1() {
        Faculty faculty = new Faculty("FCSE", new Student[]{});
        // assertThrows(RuntimeException.class, () -> {            faculty.getStudent(161528);        });
    }

    /*
     * Method: Faculty.getStudent(long)
     * Approach: Interface-based
     * C1	`students` array is not null
     * C2	`students` array is not empty
     * C3	`students` array has length greater than 1
     * C4	`students` array has no null elements
     *
     * Test Requirements: {TTTT, FFFT, TFFT, TTFT, TTTF}
     * */
    @Test
    void testGetStudent_iTTTT() {
        // this also tests the student.equals() method
        // assertEquals(this.filipMarkoski, this.faculty.getStudent(161528));
    }

    @Test
    void testGetStudent_iFFFT() {
        // null students
        // assertThrows(NullPointerException.class, () -> {            this.faculty = new Faculty("FCSE", null);        });

        // gracefully return null
        // assertNull(this.faculty.getStudent(161528));
    }

    @Test
    void testGetStudent_iTFFT() {
        // empty students
        this.faculty = new Faculty("FCSE", new Student[]{});
        assertNull(this.faculty.getStudent(161528));
    }

    @Test
    void testGetStudent_iTTFT() {
        // students with length <= 1
        this.faculty = new Faculty("FCSE", new Student[]{this.filipMarkoski});
        //assertEquals(this.filipMarkoski, this.faculty.getStudent(161528));
    }

    @Test
    void testGetStudent_iTTTF() {
        // students contains null
        // this.faculty = new Faculty("FCSE", new Student[]{null, this.filipMarkoski, null});
        // assertEquals(this.filipMarkoski, this.faculty.getStudent(161528));
    }

    /*
     * Method: Faculty.getStudent(long)
     * Approach: Functionality-based
     * C1	student with index is contained in `students`
     *
     * Test Requirements: {T, F}
     * */
    @Test
    void testGetStudent_fT() {
        // assertEquals(this.filipMarkoski, this.faculty.getStudent(161528));
    }

    @Test
    void testGetStudent_fF() {
        // assertEquals(this.filipMarkoski, this.faculty.getStudent(111111));
    }

    /**
     * Method: Faculty.getAverageNumberOfContacts()
     * Approach: Prime Path Coverage
     * Tests:
     * 1.	students=[Student(contacts=[]), Student(contacts=[])]
     * 2.	students=[]
     */
    @Test
    void testGetAverageNumberOfContacts_g1() {
        Faculty faculty = new Faculty("FCSE", new Student[]{this.filipMarkoski, this.milaVasileska});
        double sum = this.filipMarkoski.getNumberContacts() + this.milaVasileska.getNumberContacts();
        double expected = sum / 2;
        assertEquals(expected, faculty.getAverageNumberOfContacts());
    }

    @Test
    void testGetAverageNumberOfContacts_g2() {
        Faculty faculty = new Faculty("FCSE", new Student[]{});
        // assertEquals(0, faculty.getAverageNumberOfContacts());
    }

    /*
     * Method: Faculty.getAverageNumberOfContacts()
     * Approach: Interface-based
     * C1	`students` array is not null
     * C2	`students` array is not empty
     * C3	`students` array has length greater than 1
     * C4	`students` array has no null elements
     *
     * Test Requirements: {TTTT, FFFT, TFFT, TTFT, TTTF}
     * */
    @Test
    void testGetAverageNumberOfContacts_iTTTT() {
        // happy path
        double sum = this.filipMarkoski.getNumberContacts() +
                this.emilTrajanov.getNumberContacts() +
                this.milaVasileska.getNumberContacts();
        double expected = sum / this.students.size();
        assertEquals(expected, this.faculty.getAverageNumberOfContacts());
    }

    @Test
    void testGetAverageNumberOfContacts_iFFFT() {
        // null students
        // assertThrows(NullPointerException.class, () -> {            this.faculty = new Faculty("FCSE", null);        });

        // the faculty needs to be robust, no need for exceptions
        // assertThrows(NullPointerException.class, this.faculty::getAverageNumberOfContacts);
    }

    @Test
    void testGetAverageNumberOfContacts_iTFFT() {
        // empty students
        this.faculty = new Faculty("FCSE", new Student[]{});
        // assertEquals(0, this.faculty.getAverageNumberOfContacts());
    }

    @Test
    void testGetAverageNumberOfContacts_iTTFT() {
        // students with length <= 1
        this.faculty = new Faculty("FCSE", new Student[]{this.filipMarkoski});
        assertEquals(2, this.faculty.getAverageNumberOfContacts());
    }

    @Test
    void testGetAverageNumberOfContacts_iTTTF() {
        // students contains null
        // this.faculty = new Faculty("FCSE", new Student[]{null, this.filipMarkoski, null});

        // null-students should be ignored
        // assertEquals(2, this.faculty.getAverageNumberOfContacts());
    }

    /**
     * Method: Faculty.getStudentWithMostContacts()
     * Approach: Node Coverage
     * Tests:
     * 1.	students=[Student(contacts.length = 1), Student(contacts.length = 0)]
     * 2.	students=[Student(contacts.length = 0, index=111111), Student(contacts.length = 0, index=222222)]
     */
    @Test
    void testGetStudentWithMostContacts_g1() {
        this.milaVasileska.addEmailContact(date2019, email);
        Faculty faculty = new Faculty("FCSE", new Student[]{this.milaVasileska, this.emilTrajanov});
        assertEquals(this.milaVasileska.getIndex(), faculty.getStudentWithMostContacts().getIndex());
    }

    @Test
    void testGetStudentWithMostContacts_g2() {
        Faculty faculty = new Faculty("FCSE", new Student[]{this.milaVasileska, this.emilTrajanov});
        // when equal, bigger index wins
        assertEquals(this.emilTrajanov.getIndex(), faculty.getStudentWithMostContacts().getIndex());
    }

    @ParameterizedTest
    @MethodSource("getStudentWithMostContactsTRs")
    void testGetStudentWithMostContacts_l(Faculty faculty, Student expected) {
        // when equal, bigger index wins
        assertEquals(expected.getCity(), faculty.getStudentWithMostContacts().getCity());
    }

    /*
     * Method: Faculty.getStudentWithMostContacts()
     * Approach: Interface-based
     * C1	`students` array is not null
     * C2	`students` array is not empty
     * C3	`students` array has length greater than 1
     * C4	`students` array has no null elements
     *
     * Test Requirements: {TTTT, FFFT, TFFT, TTFT, TTTF}
     * */
    @Test
    void testGetStudentWithMostContacts_iTTTT() {
        // happy path
        // assertEquals(this.filipMarkoski, this.faculty.getStudentWithMostContacts());
    }

    @Test
    void testGetStudentWithMostContacts_iFFFT() {
        // null students
        // assertThrows(NullPointerException.class, () -> {            this.faculty = new Faculty("FCSE", null);        });
        // gracefully return null
        // assertNull(this.faculty.getStudentWithMostContacts());
    }

    @Test
    void testGetStudentWithMostContacts_iTFFT() {
        // empty students
        this.faculty = new Faculty("FCSE", new Student[]{});
        // gracefully return null
        // assertNull(this.faculty.getStudentWithMostContacts());
    }

    @Test
    void testGetStudentWithMostContacts_iTTFT() {
        // students with length <= 1
        this.faculty = new Faculty("FCSE", new Student[]{this.filipMarkoski});
        // assertEquals(this.filipMarkoski, this.faculty.getStudentWithMostContacts());
    }

    @Test
    void testGetStudentWithMostContacts_iTTTF() {
        // students contains null
        // this.faculty = new Faculty("FCSE", new Student[]{null, this.filipMarkoski, null});
        // assertEquals(this.filipMarkoski, this.faculty.getStudentWithMostContacts());
    }

    /**
     * Method: Faculty.toString()
     * Approach: Prime Path Coverage
     * <p>
     * Tests:
     * 1.	students=[Student(), Student()]
     * 2.	students=[]
     */
    @Test
    void testToString_g1() {
        Faculty faculty = new Faculty("FCSE", new Student[]{this.milaVasileska, this.emilTrajanov});
        String expected = "{\"fakultet\":\"FCSE\", \"studenti\":[{\"ime\":\"Mila\", \"prezime\":\"Vasileska\", \"vozrast\":19, \"grad\":\"Skopje\", \"indeks\":17246, \"telefonskiKontakti\":[], \"emailKontakti\":[]}, {\"ime\":\"Emil\", \"prezime\":\"Trajanov\", \"vozrast\":29, \"grad\":\"Bitola\", \"indeks\":24914336, \"telefonskiKontakti\":[], \"emailKontakti\":[]}]}";
        assertEquals(expected, faculty.toString());
    }

    @Test
    void testToString_g2() {
        Faculty faculty = new Faculty("FCSE", new Student[]{});
        String expected = "{\"fakultet\":\"FCSE\", \"studenti\":[]}";
        // assertEquals(expected, faculty.toString());
    }

    @Test
    void testToString() {
        // TODO: Student.toString formatting test

    }

}
