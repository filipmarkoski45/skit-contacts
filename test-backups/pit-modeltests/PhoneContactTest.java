package modeltests;

import models.Operator;
import models.PhoneContact;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


class PhoneContactTest {

    private String date;
    private String phone;
    private PhoneContact phoneContact;

    @BeforeEach
    void setUp() throws Exception {
        this.phoneContact = new PhoneContact(date, phone);
    }

    @AfterEach
    void tearDown() throws Exception {
        System.gc();
    }

    @Test
    void testPhoneContactStringString() {
        this.phoneContact = new PhoneContact(date, phone);
    }

    @Test
    void testPhoneContactPhoneContact() {
        this.phoneContact = new PhoneContact(phoneContact);
    }

    @Test
    void testGetPhone() {
        assertEquals(this.phone, this.phoneContact.getPhone());
    }

    @Test
    void testGetType() {
        assertEquals("Phone", this.phoneContact.getType());
    }

    /**
     * Method: PhoneContact.getOperator()
     * Approach: Interface-based
     * C1	`phone` is not null
     * C2	`phone` is not empty
     * C3	`phone` has a length greater or equal to 3
     * C4	`phone` contains a digit as the third literal
     * Test Requirements: TTTT, FFFF, TFFF, TTFF, TTTF
     * FTTT -> FFFF,
     * TFTT -> TFFF,
     * TTFT -> TTFF
     */
    @Test
    void testGetOperator_iTTTT() {
        // happy path

        String date = "2010-03-05";
        String phone = "070/744-235";
        PhoneContact phoneContact = new PhoneContact(date, phone);

        Operator expected = Operator.TMOBILE;
        Operator received = phoneContact.getOperator();

        assertEquals(expected, received);
    }

    @Test
    void testGetOperator_iFFFF() {
        // c.phone is null

        String date = "2010-03-05";
        String phone = null;
        PhoneContact phoneContact = new PhoneContact(date, phone);

        /* Throwing a NPE isn't descriptive, specialized exceptions need to be made */
        assertThrows(RuntimeException.class, phoneContact::getOperator);
        // fail("Throwing a NPE isn't descriptive, specialized exceptions need to be made");
    }

    @Test
    void testGetOperator_iTFFF() {
        // phone is an empty string

        String date = "2010-03-05";
        String phone = "";
        PhoneContact phoneContact = new PhoneContact(date, phone);

        assertThrows(RuntimeException.class, phoneContact::getOperator);
    }

    @Test
    void testGetOperator_iTTFF() {
        // phone.length < 3

        String date = "2010-03-05";
        String phone = "xx";
        PhoneContact phoneContact = new PhoneContact(date, phone);

        assertThrows(RuntimeException.class, phoneContact::getOperator);
    }

    @Test
    void testGetOperator_iTTTF() {
        // phone doesn't contain a digit as the third literal

        String date = "2010-03-05";
        String phone = "07x/111-111";
        PhoneContact phoneContact = new PhoneContact(date, phone);

        // assertThrows(RuntimeException.class, phoneContact::getOperator);
    }

    /**
     * Method: PhoneContact.getOperator()
     * Approach: Functionality-based
     * C1	`phone` is not TMOBILE
     * C2	`phone` is not ONE
     * C3	`phone` is not VIP
     * Test Requirements: {TTT, FTT, TFT, TTF}
     */
    @Test
    void testGetOperator_fTTT() {
        // phone is neither of the three operators

        String date = "2010-03-05";
        String phone = "079/744-235";
        PhoneContact phoneContact = new PhoneContact(date, phone);

        // assertThrows(RuntimeException.class, phoneContact::getOperator);
    }

    @Test
    void testGetOperator_fFTT() {
        // phone is TMOBILE, i.e. 0, 1, 2

        String date = "2010-03-05";
        String phone = "070/744-235";
        PhoneContact phoneContact = new PhoneContact(date, phone);

        assertEquals(Operator.TMOBILE, phoneContact.getOperator());
    }

    @Test
    void testGetOperator_fTFT() {
        // phone is ONE, i.e. 5, 6

        String date = "2010-03-05";
        String phone = "075/744-235";
        PhoneContact phoneContact = new PhoneContact(date, phone);

        assertEquals(Operator.ONE, phoneContact.getOperator());
    }

    @Test
    void testGetOperator_fTTF() {
        // phone is VIP, i.e. 7, 8

        String date = "2010-03-05";
        String phone = "077/744-235";
        PhoneContact phoneContact = new PhoneContact(date, phone);

        assertEquals(Operator.VIP, phoneContact.getOperator());
    }

    /**
     * Method: PhoneContact.getOperator()
     * Approach: Prime Path Coverage
     * 1.	`phone` containing “070/744-235”
     * 2.	`phone` containing “075/744-235”
     * 3.	`phone` containing “077/744-235”
     */

    public static List<Object[]> getOperatorTRs() {
        return Arrays.asList(new Object[][]{
                {Operator.TMOBILE, "070/744-235"},
                {Operator.ONE, "075/744-235"},
                {Operator.VIP, "077/744-235"},
        });
    }

    @ParameterizedTest
    @MethodSource("getOperatorTRs")
    public void Test(Operator expected, String phone) {
        PhoneContact phoneContact = new PhoneContact("2019-01-01", phone);
        assertEquals(expected, phoneContact.getOperator());
    }

}
